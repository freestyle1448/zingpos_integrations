package net.astropayments.nbank.payments.aspect;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import static net.astropayments.nbank.payments.models.RequestTypes.WITHDRAW;

@Aspect
@Component
@RequiredArgsConstructor
public class ScheduledRunnerTaskAspect {
    private final RunnerLogsRepository runnerLogsRepository;

    @AfterThrowing(pointcut = "within(net.astropayments.nbank.payments.tasks.paysage.ScheduledRunnerTask+)"
            , throwing = "ex")
    public void logAfterThrowingException(Exception ex) {
        runnerLogsRepository.save(RunnerLog.builder()
                .error(ex.toString())
                .fullException(ex.fillInStackTrace().toString())
                .requestType(WITHDRAW)
                .build());
    }
}
