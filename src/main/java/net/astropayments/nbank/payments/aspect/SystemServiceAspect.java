package net.astropayments.nbank.payments.aspect;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;

import net.astropayments.nbank.payments.exception.NotEnoughMoneyException;
import net.astropayments.nbank.payments.exception.NotFoundException;
import net.astropayments.nbank.payments.models.TransactionRollbackEvent;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.SystemRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class SystemServiceAspect {
    private static final Logger logger = LoggerFactory.getLogger(SystemRepository.class);
    private final ApplicationEventPublisher applicationEventPublisher;

    @Around("execution (* net.astropayments.nbank.payments.services.SystemServiceImpl.accountSub(..)) && args(transaction,..)")
    public Object loggingAccountSub(ProceedingJoinPoint joinPoint, Transaction transaction) throws Throwable {
        logger.info("Начало списания средств с аккаунта");

        Object proceed;
        final var event = new TransactionRollbackEvent(transaction, false);
        try {
            applicationEventPublisher.publishEvent(event);
            proceed = joinPoint.proceed();
        } catch (NotEnoughMoneyException ex) {
            logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (NotFoundException ex) {
            logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Ошибка {}! Для транзакции {}", ex, transaction);

            event.setIsManualException(true);
            throw ex;
        }

        logger.info("Закончили списание для - {}", transaction);
        return proceed;
    }

    @Around("execution(* net.astropayments.nbank.payments.services.SystemServiceImpl.declineTransaction(..)) && args(transaction,..)")
    public Object loggingDecline(ProceedingJoinPoint joinPoint, Transaction transaction) throws Throwable {
        logger.info("Начало отмены транзакции");

        Object proceed;
        final var event = new TransactionRollbackEvent(transaction, false);
        try {
            applicationEventPublisher.publishEvent(event);
            proceed = joinPoint.proceed();
        } catch (NotFoundException ex) {
            logger.error("Во время отмены транзакции аккаунт не был найден! - {}", transaction);

            event.setIsManualException(true);
            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Ошибка {}! Для транзакции {}", ex, transaction);

            event.setIsManualException(true);
            throw ex;
        }

        logger.error("Успешный возврат! - id- {}", transaction);
        return proceed;
    }

    @Around("execution(* net.astropayments.nbank.payments.services.AdminServiceImpl.accountAdd(..))")
    public Object loggingAccountAdd(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("Обработка операции на ввод");

        Object proceed;
        try {
            proceed = joinPoint.proceed();
        } catch (NotFoundException ex) {
            logger.error("{}\n", ex.getLocalizedMessage());

            throw ex;
        } catch (MongoException | DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Пополнение не удалось!", ex);

            throw ex;
        }

        logger.info("Пополнение аккаунта успешно");

        return proceed;
    }
}
