package net.astropayments.nbank.payments.repositories;

import net.astropayments.nbank.payments.models.Account;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

import static net.astropayments.nbank.payments.models.transaction.Status.*;
import static net.astropayments.nbank.payments.models.transaction.Type.USER_OUT;


@Repository
public class ManualRepository {
    public static final String GATE_ID = "gateId";
    public static final String STATUS = "status";
    public static final String STAGE = "stage";
    private static final String BALANCE_AMOUNT = "balance.amount";
    private static final String BALANCE_CURRENCY = "balance.currency";
    public static final String TRANSACTION_NUMBER = "transactionNumber";

    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        var findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        var update = new Update();
        update.inc(BALANCE_AMOUNT, balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, new FindAndModifyOptions().returnNew(true), Account.class);
    }

    public void saveTransaction(Transaction transaction) {
        var findAndModifyTransaction = new Query(Criteria
                .where(TRANSACTION_NUMBER).is(transaction.getTransactionNumber()));
        var update = new Update();
        update.set(STATUS, transaction.getStatus());
        update.set(STAGE, transaction.getStage());

        if (transaction.getErrorReason() != null) {
            update.set("errorReason", transaction.getErrorReason());
            update.set("systemError", transaction.getSystemError());
        }
        if (transaction.getEndDate() != null) {
            update.set("endDate", transaction.getEndDate());
        }

        mongoTemplate.findAndModify(findAndModifyTransaction, update, Transaction.class);
    }

    public Account findAndModifyAccountSub(String accountId, Balance balance) {
        var findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        var update = new Update();
        update.inc(BALANCE_AMOUNT, -balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, FindAndModifyOptions.options().returnNew(true), Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and(BALANCE_CURRENCY).is(balance.getCurrency()));
        Update update = new Update();
        update.inc(BALANCE_AMOUNT, balance.getAmount());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public void findAndModifyGateSet(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId));
        Update update = new Update();
        update.set(BALANCE_AMOUNT, balance.getAmount());

        mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public List<Transaction> findTransactionsInWork(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(IN_PROCESS)
                .and(STAGE).is(1)
                .and(GATE_ID).in(gates)
                .and("type").is(USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findNBManualTransactions(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(MANUAL)
                .and(STAGE).is(0)
                .and(GATE_ID).in(gates)
                .and("type").is(USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findTransactionAfterError(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(IN_PROCESS)
                .and(STAGE).is(3)
                .and(GATE_ID).in(gates)
                .and("type").is(USER_OUT)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public Transaction findAndModifyTransaction(List<ObjectId> gates) {
        Query query = new Query(Criteria
                .where(STATUS).is(WAITING)
                .and(GATE_ID).in(gates)
                .and(STAGE).is(0)
                .and(TRANSACTION_NUMBER).ne(null)
                .and("type").is(USER_OUT));
        Update update = new Update();
        update.inc(STAGE, 1);

        return mongoTemplate.findAndModify(query, update,
                FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }

    public Transaction findAndModifyTransaction(ObjectId objectId) {
        Query query = new Query(Criteria
                .where("_id").is(objectId));
        Update update = new Update();
        update.inc(STAGE, 1);

        return mongoTemplate.findAndModify(query, update,
                FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }

    public List<Gate> findPsbGates() {
        var find = new Query(Criteria
                .where("group").is("psb"));

        return mongoTemplate.find(find, Gate.class);
    }

    public List<Gate> findViaGates() {
        var find = new Query(Criteria
                .where("group").is("via_payments"));

        return mongoTemplate.find(find, Gate.class);
    }

    public Gate findGateAndSetBalance(ObjectId objectId, Long balance) {
        var find = new Query(Criteria.where("_id").is(objectId));
        var update = new Update()
                .set(BALANCE_AMOUNT, balance);

        return mongoTemplate.findAndModify(find, update, new FindAndModifyOptions().returnNew(true), Gate.class);
    }
}
