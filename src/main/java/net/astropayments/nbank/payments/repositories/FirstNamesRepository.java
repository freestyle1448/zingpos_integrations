package net.astropayments.nbank.payments.repositories;

import net.astropayments.nbank.payments.models.FirstName;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FirstNamesRepository extends MongoRepository<FirstName, ObjectId> {

}
