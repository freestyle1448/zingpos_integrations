package net.astropayments.nbank.payments.repositories;

import net.astropayments.nbank.payments.models.RunnerLog;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RunnerLogsRepository extends MongoRepository<RunnerLog, ObjectId> {
}
