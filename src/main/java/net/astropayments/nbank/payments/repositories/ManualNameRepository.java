package net.astropayments.nbank.payments.repositories;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.FirstName;
import net.astropayments.nbank.payments.models.LastName;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Random;

@Repository
@RequiredArgsConstructor
public class ManualNameRepository {
    public static final int FIRST_NAMES_SIZE = 5162;
    public static final int LAST_NAMES_SIZE = 88799;
    private final Random rand = new Random();
    private final MongoTemplate mongoTemplate;

    public String getFirstName() {
        Query query = new Query();
        query.limit(1);
        query.skip(rand.nextInt(FIRST_NAMES_SIZE));
        return mongoTemplate.find(query, FirstName.class).get(0).getValue();
    }

    public String getLastName() {
        Query query = new Query();
        query.limit(1);
        query.skip(rand.nextInt(LAST_NAMES_SIZE));
        return mongoTemplate.find(query, LastName.class).get(0).getValue();
    }
}
