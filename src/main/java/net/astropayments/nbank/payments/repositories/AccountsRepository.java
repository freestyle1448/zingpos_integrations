package net.astropayments.nbank.payments.repositories;


import net.astropayments.nbank.payments.models.Account;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AccountsRepository extends MongoRepository<Account, ObjectId> {
    Optional<Account> findByAccountId(String accountId);
}
