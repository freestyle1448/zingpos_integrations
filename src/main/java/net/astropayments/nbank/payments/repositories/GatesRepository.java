package net.astropayments.nbank.payments.repositories;


import net.astropayments.nbank.payments.models.Gate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
    Gate findByName(String name);

    Optional<Gate> findByWithdrawalMethod(String withdrawalMethod);
}

