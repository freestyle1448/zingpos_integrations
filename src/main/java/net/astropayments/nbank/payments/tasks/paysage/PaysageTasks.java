package net.astropayments.nbank.payments.tasks.paysage;

import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import net.astropayments.nbank.payments.services.RunnerService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static net.astropayments.nbank.payments.models.RequestTypes.CHECK;
import static net.astropayments.nbank.payments.models.RequestTypes.WITHDRAW;
import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;

@Component
public class PaysageTasks {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");
    private final RunnerLogsRepository runnerLogsRepository;

    private final ManualRepository manualRepository;
    private final RunnerService runnerService;
    private final GatesRepository gatesRepository;

    public PaysageTasks(RunnerLogsRepository runnerLogsRepository, ManualRepository manualRepository,
                        @Qualifier("paysageServiceImpl") RunnerService runnerService, GatesRepository gatesRepository) {
        this.runnerLogsRepository = runnerLogsRepository;
        this.manualRepository = manualRepository;
        this.runnerService = runnerService;
        this.gatesRepository = gatesRepository;
    }

    @Async
    public void payAfterError() {
        try {
            List<Transaction> transactions = new ArrayList<>();
            Transaction tr;
            List<ObjectId> gatesId = manualRepository.findViaGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            try {
                tr = manualRepository.findAndModifyTransaction(gatesId);
            } catch (Exception ex) {
                tr = Transaction.builder().build();
            }


            while (tr != null) {
                transactions.add(tr);

                try {
                    tr = manualRepository.findAndModifyTransaction(gatesId);
                } catch (Exception ex) {
                    tr = Transaction.builder().build();
                    continue;
                }
            }

            CompletableFuture<?>[] tasks = transactions
                    .stream()
                    .map(transaction -> {
                        try {
                            logger.info("Вызов метода withdraw для транзакции - {}", transaction);

                            if (transaction.getGateId() != null) {
                                final var optionalGate = gatesRepository.findById(transaction.getGateId());

                                if (optionalGate.isEmpty())
                                    return CompletableFuture.completedFuture(null);

                                runnerService.withdraw(transaction, optionalGate.get());
                                return CompletableFuture.completedFuture(transaction);
                            }
                            return CompletableFuture.completedFuture(null);
                        } catch (Exception exc) {
                            saveTransactionAfterException(transaction, 0, WAITING);

                            logger.error("Во время выполнения запроса на вывод возникла ошибка!", exc);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            runnerLogsRepository.save(RunnerLog.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(WITHDRAW)
                    .build());
        }
    }

    //@Scheduled(fixedDelay = 300000, initialDelay = 1000)
    public void pay() {
        try {
            List<ObjectId> gatesId = manualRepository.findViaGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findTransactionAfterError(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .stream()
                    .map(transaction -> {
                        try {
                            logger.info("Вызов метода withdraw для транзакции - {}", transaction);

                            if (transaction.getGateId() != null) {
                                final var optionalGate = gatesRepository.findById(transaction.getGateId());

                                if (optionalGate.isEmpty())
                                    return CompletableFuture.completedFuture(null);

                                runnerService.withdraw(transaction, optionalGate.get());
                                return CompletableFuture.completedFuture(transaction);
                            }
                            return CompletableFuture.completedFuture(null);
                        } catch (Exception exc) {
                            saveTransactionAfterException(transaction, 0, WAITING);

                            logger.error("Во время выполнения запроса на вывод возникла ошибка!", exc);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            runnerLogsRepository.save(RunnerLog.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(WITHDRAW)
                    .build());
        }
    }

    //@Scheduled(fixedDelay = 10000, initialDelay = 1000)
    public void checkTransactions() {
        try {
            List<ObjectId> gatesId = manualRepository.findViaGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findTransactionsInWork(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .parallelStream()
                    .map(transaction -> {
                        try {
                            final var gateOptional = gatesRepository.findById(transaction.getGateId());
                            if (gateOptional.isPresent()) {
                                runnerService.checkTransaction(transaction, gateOptional.get());

                                return CompletableFuture.completedFuture(transaction);
                            } else {
                                logger.error("Во время выполнения запроса на проверку статуса возникла ошибка! Гейт для транзакции не найден! {}", transaction);

                                return null;
                            }
                        } catch (Exception exc) {
                            logger.error("Во время выполнения запроса на проверку статуса возникла ошибка!", exc);

                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            runnerLogsRepository.save(RunnerLog.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(CHECK)
                    .build());
        }
    }

    //@Scheduled(fixedDelay = 60000, initialDelay = 1000)
    public void checkManualTransactions() {
        try {
            List<ObjectId> gatesId = manualRepository.findViaGates()
                    .stream()
                    .map(Gate::getId)
                    .collect(Collectors.toList());

            List<Transaction> transactions = manualRepository.findTransactionsInWork(gatesId);

            CompletableFuture<?>[] tasks = transactions
                    .parallelStream()
                    .map(transaction -> {
                        try {
                            final var gateOptional = gatesRepository.findById(transaction.getGateId());
                            if (gateOptional.isPresent()) {
                                runnerService.checkTransaction(transaction, gateOptional.get());

                                return CompletableFuture.completedFuture(transaction);
                            } else {
                                logger.error("Во время выполнения запроса на проверку статуса возникла ошибка! Гейт для транзакции не найден! {}", transaction);

                                return null;
                            }
                        } catch (Exception exc) {
                            logger.error("Во время выполнения запроса на проверку статуса возникла ошибка!", exc);

                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .get();
        } catch (Exception ex) {
            runnerLogsRepository.save(RunnerLog.builder()
                    .error(ex.toString())
                    .fullException(ex.fillInStackTrace().toString())
                    .requestType(CHECK)
                    .build());
        }
    }

    //TODO сделать пинг для paysage
    //@Scheduled(fixedDelay = 1800000)
    public void ping() {
        runnerService.ping();
    }

    private void saveTransactionAfterException(Transaction transaction, int stage, int status) {
        transaction.setStage(stage);
        transaction.setStatus(status);

        manualRepository.saveTransaction(transaction);
    }
}
