package net.astropayments.nbank.payments.tasks.paysage;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.services.RunnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;

@Component
@RequiredArgsConstructor
public class PaysageRunnerTasks implements ScheduledRunnerTask {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final ManualRepository manualRepository;
    @Qualifier("paysageServiceImpl")
    private final RunnerService runnerService;
    private final GatesRepository gatesRepository;

    @Async
    @Override
    public void handlePayTransactionsAfterRestart() {
        final var transactions = new ArrayList<Transaction>();
        final var gatesId = manualRepository.findViaGates()
                .stream()
                .map(Gate::getId)
                .collect(Collectors.toList());

        var tr = manualRepository.findAndModifyTransaction(gatesId);
        while (tr != null) {
            transactions.add(tr);

            tr = manualRepository.findAndModifyTransaction(gatesId);
        }

        transactions.forEach(transaction -> {
            logger.info("Вызов метода handlePayTransactionsAfterRestart для транзакции - {}", transaction);

            if (transaction.getGateId() != null) {
                gatesRepository.findById(transaction.getGateId())
                        .ifPresentOrElse(gate -> runnerService.withdraw(transaction, gate)
                                , () -> saveTransactionAfterError(transaction, 0, WAITING));
            }
        });
    }

    @Override
    @Scheduled(fixedDelay = 300000)
    public void handlePayTransactionsAfterSystemError() {
        final var gatesId = manualRepository.findViaGates()
                .stream()
                .map(Gate::getId)
                .collect(Collectors.toList());

        final var transactions = manualRepository.findTransactionAfterError(gatesId);

        transactions.forEach(transaction -> {
            logger.info("Вызов метода handlePayTransactionsAfterRestart для транзакции - {}", transaction);

            if (transaction.getGateId() != null) {
                gatesRepository.findById(transaction.getGateId())
                        .ifPresentOrElse(gate -> runnerService.withdraw(transaction, gate)
                                , () -> saveTransactionAfterError(transaction, 0, WAITING));
            }
        });
    }

    @Override
    @Scheduled(fixedDelay = 10000)
    public void checkTransactionsStatus() {
        final var gatesId = manualRepository.findViaGates()
                .stream()
                .map(Gate::getId)
                .collect(Collectors.toList());

        final var transactions = manualRepository.findTransactionsInWork(gatesId);

        transactions.forEach(transaction -> {
            logger.info("Вызов метода checkTransactionsStatus для транзакции - {}", transaction);

            if (transaction.getGateId() != null) {
                gatesRepository.findById(transaction.getGateId())
                        .ifPresentOrElse(gate -> runnerService.checkTransaction(transaction, gate)
                                , () -> saveTransactionAfterError(transaction, 0, WAITING));
            }
        });
    }

    @Override
    public void checkManualTransactionsStatus() {
        //TODO при появлении у Paysage транзакций со stage 5 добавить
    }

    private void saveTransactionAfterError(Transaction transaction, int stage, int status) {
        transaction.setStage(stage);
        transaction.setStatus(status);

        manualRepository.saveTransaction(transaction);
    }
}
