package net.astropayments.nbank.payments.tasks.paysage;

public interface ScheduledRunnerTask {
    void handlePayTransactionsAfterRestart();

    void handlePayTransactionsAfterSystemError();

    void checkTransactionsStatus();

    void checkManualTransactionsStatus();
}
