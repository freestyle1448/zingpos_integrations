package net.astropayments.nbank.payments.tasks.paysage;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import net.astropayments.nbank.payments.models.RequestTypes;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import net.astropayments.nbank.payments.services.RunnerService;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.messaging.ChangeStreamRequest;
import org.springframework.data.mongodb.core.messaging.MessageListener;
import org.springframework.data.mongodb.core.messaging.MessageListenerContainer;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;
import static net.astropayments.nbank.payments.models.transaction.Type.USER_OUT;

@Component
public class PaysageStreams {
    public static final String STATUS = "status";
    public static final String STAGE = "stage";
    public static final String TRANSACTIONS = "transactions";

    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");
    private final RunnerLogsRepository runnerLogsRepository;

    private final GatesRepository gatesRepository;
    private final ManualRepository manualRepository;
    private final MessageListenerContainer messageListenerContainer;
    private final RunnerService runnerService;
    //private final PaysageTasks paysageTasks;
    private final ScheduledRunnerTask paysageTasks;

    public PaysageStreams(RunnerLogsRepository runnerLogsRepository, GatesRepository gatesRepository,
                          ManualRepository manualRepository, MessageListenerContainer messageListenerContainer,
                          @Qualifier("paysageServiceImpl") RunnerService runnerService, ScheduledRunnerTask paysageTasks) {
        this.runnerLogsRepository = runnerLogsRepository;
        this.gatesRepository = gatesRepository;
        this.manualRepository = manualRepository;
        this.messageListenerContainer = messageListenerContainer;
        this.runnerService = runnerService;
        this.paysageTasks = paysageTasks;
    }

    @PostConstruct
    public void postConstruct() {
        try {
            paysageTasks.handlePayTransactionsAfterRestart();
        } catch (Exception e) {
            logger.error("Ошибка при вызове метода восстановления после сбоя!", e);
        }

        subscribeToPaysageOut();
    }

    public void subscribeToPaysageOut() {
        final var nbOutTransaction = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("type").is(USER_OUT)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    final var gateOptional = gatesRepository.findById(newTransaction.getGateId());

                    if (gateOptional.isPresent()) {
                        final var gate = gateOptional.get();

                        if (gate.getGroup().equals("via_payments")) {
                            final var nbTransactionUpdated = manualRepository.findAndModifyTransaction(newTransaction.getId());
                            logger.info("Вызов метода out для транзакции - {}", newTransaction);

                            runnerService.withdraw(nbTransactionUpdated, gate);
                        }
                    }
                }
            } catch (Exception ex) {
                runnerLogsRepository.save(RunnerLog.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(RequestTypes.WITHDRAW)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .filter(nbOutTransaction)
                .build();

        messageListenerContainer.start();
        messageListenerContainer.register(request, Transaction.class);
    }
}
