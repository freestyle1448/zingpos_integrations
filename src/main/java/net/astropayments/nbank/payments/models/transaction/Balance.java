package net.astropayments.nbank.payments.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Balance {
    private Long amount;
    private String currency;

    public Balance add(Balance add2) {
        return Balance.builder()
                .currency(this.getCurrency())
                .amount(this.getAmount() + add2.getAmount())
                .build();
    }
}
