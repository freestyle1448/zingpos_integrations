package net.astropayments.nbank.payments.models.psb.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class File {
    private String privateKeyPath;
    private String publicKeyPath;
}
