package net.astropayments.nbank.payments.models;

public final class GateTypes {
    public static final Integer AUTO = 1;
    public static final Integer MANUAL = 0;
}
