package net.astropayments.nbank.payments.models;

import lombok.Value;

import java.util.AbstractMap;
import java.util.List;
import java.util.Random;

@Value
public class NamesHolder {
    Random rand = new Random();
    List<String> firstNames;
    List<String> lastNames;

    public AbstractMap.SimpleEntry<String, String> getRandomFirstNameWithSecond() {
        return new AbstractMap.SimpleEntry<>(firstNames.get(rand.nextInt(firstNames.size())), lastNames.get(rand.nextInt(lastNames.size())));
    }
}
