package net.astropayments.nbank.payments.models;

import lombok.Value;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Document(collection = "first_names")
public class FirstName {
    ObjectId id;
    String value;
}
