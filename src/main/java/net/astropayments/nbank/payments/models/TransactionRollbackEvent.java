package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.astropayments.nbank.payments.models.transaction.Transaction;


@Data
@AllArgsConstructor
public class TransactionRollbackEvent {
    private Transaction transaction;
    private Boolean isManualException;
}
