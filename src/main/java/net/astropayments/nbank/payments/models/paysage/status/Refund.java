package net.astropayments.nbank.payments.models.paysage.status;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Refund {
    @JsonProperty("auth_code")
    private String authCode;
    private String rrn;
    @JsonProperty("ref_id")
    private String refId;
    private String message;
    @JsonProperty("gateway_id")
    private Long gatewayId;
    private String status;
}
