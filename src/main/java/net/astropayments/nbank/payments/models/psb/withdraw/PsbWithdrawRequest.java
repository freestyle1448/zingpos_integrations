package net.astropayments.nbank.payments.models.psb.withdraw;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PsbWithdrawRequest {
    private String AMOUNT;
    @Builder.Default
    private String CURRENCY = "RUB";
    private String ORDER;
    private String DESC;
    private String TERMINAL;
    @Builder.Default
    private String TRTYPE = "70";
    private String MERCH_NAME;
    private String MERCHANT;
    private String EMAIL;
    private String TIMESTAMP;
    private String NONCE;
    @Builder.Default
    private String BACKREF = "https://zingpay.ru";
    private String P_SIGN;
    private String CARD;
    @Builder.Default
    private String PAYMENT_RECEIVER_FIRST_NAME = "Igor";
    @Builder.Default
    private String PAYMENT_RECEIVER_MIDDLE_NAME = "Davidovich";
    @Builder.Default
    private String PAYMENT_RECEIVER_LAST_NAME = "Orlov";
    @Builder.Default
    private String PAYMENT_RECEIVER_STREET = "Izmaylovskaya";
    @Builder.Default
    private String PAYMENT_RECEIVER_CITY = "Moscow";
    @Builder.Default
    private String PAYMENT_RECEIVER_STATE_CODE = "RU";
    @Builder.Default
    private String PAYMENT_RECEIVER_COUNTRY = "RUS";
    @Builder.Default
    private String PAYMENT_RECEIVER_POSTAL_CODE = "144422";
    @Builder.Default
    private String PAYMENT_RECEIVER_PHONE = "+79777777777";
    @Builder.Default
    private String PAYMENT_RECEIVER_DATE_OF_BIRTH = "01011991";
    @Builder.Default
    private String PAYMENT_RECEIVER_IDENTITY_TYPE = "00";
    @Builder.Default
    private String PAYMENT_RECEIVER_IDENTITY_ID = "051169";
    @Builder.Default
    private String PAYMENT_RECEIVER_IDENTITY_COUNTRY = "RUS";
    @Builder.Default
    private String PAYMENT_RECEIVER_IDENTITY_EXP_DATE = "09072043";
    @Builder.Default
    private String PAYMENT_RECEIVER_NATIONALITY = "RUS";
    @Builder.Default
    private String PAYMENT_RECEIVER_COUNTRY_OF_BIRTH = "RUS";
    @Builder.Default
    private String PAYMENT_SENDER_FIRST_NAME = "Zing";
    @Builder.Default
    private String PAYMENT_SENDER_MIDDLE_NAME = "NONE";
    @Builder.Default
    private String PAYMENT_SENDER_LAST_NAME = "Pay";
    @Builder.Default
    private String PAYMENT_SENDER_STREET = "Doroga";
    @Builder.Default
    private String PAYMENT_SENDER_CITY = "Shatrovo";
    @Builder.Default
    private String PAYMENT_SENDER_STATE_CODE = "RU";
    @Builder.Default
    private String PAYMENT_SENDER_COUNTRY = "RUS";
    @Builder.Default
    private String PAYMENT_SENDER_POSTAL_CODE = "146522";
    @Builder.Default
    private String PAYMENT_SENDER_PHONE = "+79776666666";
    @Builder.Default
    private String PAYMENT_SENDER_DATE_OF_BIRTH = "02021992";
    @Builder.Default
    private String PAYMENT_SENDER_IDENTITY_TYPE = "00";
    @Builder.Default
    private String PAYMENT_SENDER_IDENTITY_ID = "010169";
    @Builder.Default
    private String PAYMENT_SENDER_IDENTITY_COUNTRY = "RUS";
    @Builder.Default
    private String PAYMENT_SENDER_IDENTITY_EXP_DATE = "09072043";
    @Builder.Default
    private String PAYMENT_SENDER_NATIONALITY = "RUS";
    @Builder.Default
    private String PAYMENT_SENDER_COUNTRY_OF_BIRTH = "RUS";
    @Builder.Default
    private String PAYMENT_SENDER_ACCOUNT_NUMBER = "NONE";
    @Builder.Default
    private String PAYMENT_TYPE_ID = "PUB";

    public String createStringSign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(AMOUNT != null && !AMOUNT.isEmpty() ? String.format("%d%s", AMOUNT.length(), AMOUNT) : "-");
        signBuilder.append(CURRENCY != null && !CURRENCY.isEmpty() ? String.format("%d%s", CURRENCY.length(), CURRENCY) : "-");
        signBuilder.append(ORDER != null && !ORDER.isEmpty() ? String.format("%d%s", ORDER.length(), ORDER) : "-");
        signBuilder.append(MERCH_NAME != null && !MERCH_NAME.isEmpty() ? String.format("%d%s", MERCH_NAME.length(), MERCH_NAME) : "-");
        signBuilder.append(MERCHANT != null && !MERCHANT.isEmpty() ? String.format("%d%s", MERCHANT.length(), MERCHANT) : "-");
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(EMAIL != null && !EMAIL.isEmpty() ? String.format("%d%s", EMAIL.length(), EMAIL) : "-");
        signBuilder.append(TRTYPE != null && !TRTYPE.isEmpty() ? String.format("%d%s", TRTYPE.length(), TRTYPE) : "-");
        signBuilder.append(TIMESTAMP != null && !TIMESTAMP.isEmpty() ? String.format("%d%s", TIMESTAMP.length(), TIMESTAMP) : "-");
        signBuilder.append(NONCE != null && !NONCE.isEmpty() ? String.format("%d%s", NONCE.length(), NONCE) : "-");
        signBuilder.append(BACKREF != null && !BACKREF.isEmpty() ? String.format("%d%s", BACKREF.length(), BACKREF) : "-");

        return signBuilder.toString();
    }


    public MultiValueMap<String, String> requestData() throws IllegalAccessException {
        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.get(this) == null)
                continue;
            map.add(field.getName(), String.valueOf(field.get(this)));
        }

        return map;
    }
}
