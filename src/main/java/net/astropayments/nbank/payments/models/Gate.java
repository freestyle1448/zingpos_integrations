package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.models.transaction.Commission;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "gates")
public class Gate {
    @Id
    private ObjectId id;
    private String name;
    private Integer type;
    private String currency;
    private Commission commission;
    private Balance balance;
    private String account;
    private String withdrawalMethod;
    private PaysageCredentials paysageCredentials;
    private PsbCredentials psbCredentials;
    private NbCredentials nbCredentials;
    private String group;
}
