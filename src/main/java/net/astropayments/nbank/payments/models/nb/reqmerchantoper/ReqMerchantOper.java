package net.astropayments.nbank.payments.models.nb.reqmerchantoper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.NbCredentials;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReqMerchantOper {
    @JsonProperty("CustomerId")
    private String customerId;
    @JsonProperty("ReqId")
    private String reqId;
    @JsonProperty("Token")
    private String token;
    @JsonProperty("SignedData")
    private String signedData;
    @JsonProperty("Sign")
    private String sign;

    @JsonIgnore
    private ReqMerchantOperData signedDataObject;
    @JsonIgnore
    private NbCredentials nbCredentials;
}
