package net.astropayments.nbank.payments.models.nb.reqmerchantoper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReqMerchantOperResponse {
    @JsonProperty("RespId")
    private final String respId;
    @JsonProperty("SignedData")
    private final String signedData;
    @JsonProperty("Sign")
    private final String sign;

    @JsonIgnore()
    private ReqMerchantOperResponseData data;
}
