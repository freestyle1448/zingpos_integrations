package net.astropayments.nbank.payments.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "mongo")
public class MongoProps {
    private String credPath;
}
