package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "worker_logs")
public class RunnerLog {
    @Id
    private ObjectId id;
    private Date date;
    private String requestType;
    private String error;
    private String fullException;
    private String transactionNumber;
    private String request;
    private String response;
}
