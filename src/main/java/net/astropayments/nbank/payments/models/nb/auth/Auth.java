package net.astropayments.nbank.payments.models.nb.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Auth {
    @JsonProperty("CustomerId")
    private final String customerId;
    @JsonProperty("Password")
    private final String password;
    @JsonProperty("ReqId")
    private final String reqId;
}
