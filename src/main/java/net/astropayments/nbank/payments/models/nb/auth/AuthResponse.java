package net.astropayments.nbank.payments.models.nb.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class AuthResponse {
    @JsonProperty("RespId")
    private final String respId;
    @JsonProperty("Token")
    private final String token;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss", timezone = "Europe/Moscow")
    @JsonProperty("DateExp")
    private final Date dateExp;
    @JsonProperty("ErrCode")
    private final String errCode;
    @JsonProperty("ErrText")
    private final String errText;
    @JsonProperty("Error")
    private final Boolean error;
}
