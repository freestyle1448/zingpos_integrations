package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaysageCredentials {
    private Boolean isTest;
    private String shopId;
    private String secret;
}
