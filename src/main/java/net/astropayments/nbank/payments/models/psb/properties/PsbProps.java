package net.astropayments.nbank.payments.models.psb.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "psb")
public class PsbProps {
    private File file;
    private Url url;
}
