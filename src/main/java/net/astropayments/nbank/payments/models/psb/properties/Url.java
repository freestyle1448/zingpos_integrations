package net.astropayments.nbank.payments.models.psb.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Url {
    private String withdraw;
    private String check;
    private String ping;
}
