package net.astropayments.nbank.payments.models.paysage.payouts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Payout {
    @JsonProperty("auth_code")
    private String authCode;
    @JsonProperty("bank_code")
    private String bankCode;
    private String rrn;
    @JsonProperty("ref_id")
    private String refId;
    private String message;
    @JsonProperty("gateway_id")
    private Long gatewayId;
    @JsonProperty("billing_descriptor")
    private String billingDescriptor;
    private String status;
}
