package net.astropayments.nbank.payments.models.transaction;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

@Document(collection = "transactions")
public class Transaction {
    @Id
    private ObjectId id;
    private String sign;
    private String salt;
    private String hash;
    private ObjectId gateId;
    private Date startDate;
    private Date endDate;
    private Long transactionNumber;
    private Integer status;
    private Integer stage;
    private String type;
    private Balance gateAmount;
    private Balance amount;
    private Balance commission;
    private Balance finalAmount;
    private String purpose;
    private String note;
    private Integer gateType;
    private UserCredentials senderCredentials;
    private UserCredentials receiverCredentials;
    private String withdrawalMethod;
    private String errorReason;
    private String systemError;
    private Balance bankCommission;
    private Balance systemCommission;
    private String amountMethod;
}
