package net.astropayments.nbank.payments.models;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonRootName("answer")
public class Answer {
    private String status;
    private String err;
    private Long amount;
}
