package net.astropayments.nbank.payments.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Commission {
    private Long min;
    private Double percent;
    private Long max;
    private Long fee;
}
