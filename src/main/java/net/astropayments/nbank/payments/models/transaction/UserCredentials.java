package net.astropayments.nbank.payments.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCredentials {
    private String account;
    private String email;
    private String cardNumber;
    private String phone;
    private String year;
    private String month;
    private String userPhone;
    private Long value;
    private String destination;
    private ObjectId gate;
}
