package net.astropayments.nbank.payments.models.psb.check;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class PsbCheckResponse {
    @JsonProperty("AMOUNT")
    private String amount;
    @JsonProperty("ORG_AMOUNT")
    private String orgAmount;
    @JsonProperty("CURRENCY")
    private String currency;
    @JsonProperty("ORDER")
    private String order;
    @JsonProperty("DESC")
    private String desc;
    @JsonProperty("MERCH_NAME")
    private String merchName;
    @JsonProperty("MERCHANT")
    private String merchant;
    @JsonProperty("TERMINAL")
    private String terminal;
    @JsonProperty("EMAIL")
    private String email;
    @JsonProperty("TRTYPE")
    private String trtype;
    @JsonProperty("TIMESTAMP")
    private String timestamp;
    @JsonProperty("NONCE")
    private String nonce;
    @JsonProperty("BACKREF")
    private String backref;
    @JsonProperty("RESULT")
    private String result;
    @JsonProperty("RC")
    private String rc;
    @JsonProperty("RCTEXT")
    private String rctext;
    @JsonProperty("AUTHCODE")
    private String authcode;
    @JsonProperty("RRN")
    private String rrn;
    @JsonProperty("INT_REF")
    private String intRef;
    @JsonProperty("NAME")
    private String name;
    @JsonProperty("CARD")
    private String card;
    @JsonProperty("P_SIGN")
    private String pSign;
    @JsonProperty("ERROR")
    private String error;
}
