package net.astropayments.nbank.payments.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PsbCredentials {
    private String terminal;
    private String merchantName;
    private String merchant;
}
