package net.astropayments.nbank.payments.models.paysage.payouts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.paysage.CreditCard;
import net.astropayments.nbank.payments.models.paysage.Recipient;
import net.astropayments.nbank.payments.models.paysage.RecipientBillingAddress;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("transaction")
public class PayoutResponse {
    private String uid;
    private String status;
    private String message;
    private Long amount;
    private String currency;
    private String description;
    private String type;
    @JsonProperty("tracking_id")
    private String trackingId;
    private String language;
    private Recipient recipient;
    @JsonProperty("credit_card")
    private CreditCard creditCard;
    @JsonProperty("recipient_billing_address")
    private RecipientBillingAddress recipientBillingAddress;
    private Payout payout;
}
