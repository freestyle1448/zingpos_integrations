package net.astropayments.nbank.payments.models.paysage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCard {
    private String holder;
    private String stamp;
    private String token;
    private String brand;
    @JsonProperty("last_4")
    private String last4;
    @JsonProperty("first_1")
    private String first1;
    @JsonProperty("exp_month")
    private Integer expMonth;
    @JsonProperty("exp_year")
    private Integer expYear;
}
