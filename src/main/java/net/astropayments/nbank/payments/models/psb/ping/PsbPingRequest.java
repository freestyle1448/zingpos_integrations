package net.astropayments.nbank.payments.models.psb.ping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PsbPingRequest {
    private String TERMINAL;
    private String TIMESTAMP;
    private String NONCE;
    private String P_SIGN;

    @JsonIgnore
    private ObjectId gateId;

    public String createStringSign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(TIMESTAMP != null && !TIMESTAMP.isEmpty() ? String.format("%d%s", TIMESTAMP.length(), TIMESTAMP) : "-");
        signBuilder.append(NONCE != null && !NONCE.isEmpty() ? String.format("%d%s", NONCE.length(), NONCE) : "-");

        return signBuilder.toString();
    }
}
