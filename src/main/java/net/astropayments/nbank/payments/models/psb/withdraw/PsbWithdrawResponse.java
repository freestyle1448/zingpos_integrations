package net.astropayments.nbank.payments.models.psb.withdraw;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class PsbWithdrawResponse {
    @JsonProperty("AMOUNT")
    private String amount;
    @JsonProperty("CURRENCY")
    private String currency;
    @JsonProperty("ORDER")
    private String order;
    @JsonProperty("DESC")
    private String desc;
    @JsonProperty("TERMINAL")
    private String terminal;
    @JsonProperty("TRTYPE")
    private String trtype;
    @JsonProperty("MERCH_NAME")
    private String merchName;
    @JsonProperty("MERCHANT")
    private String merchant;
    @JsonProperty("EMAIL")
    private String email;
    @JsonProperty("TIMESTAMP")
    private String timestamp;
    @JsonProperty("NONCE")
    private String nonce;
    @JsonProperty("BACKREF")
    private String backref;
    @JsonProperty("NAME")
    private String name;
    @JsonProperty("CARD")
    private String card;
    @JsonProperty("RC")
    private String rc;
    @JsonProperty("RCTEXT")
    private String rctext;
    @JsonProperty("AUTHCODE")
    private String authcode;
    @JsonProperty("RRN")
    private String rrn;
    @JsonProperty("INT_REF")
    private String intRef;
    @JsonProperty("RESULT")
    private String result;
    @JsonProperty("P_SIGN")
    private String pSign;
    @JsonProperty("ERROR")
    private String error;

    @SuppressWarnings("Duplicates")
    public String getSignData() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(amount != null && !amount.isEmpty() ? String.format("%d%s", amount.length(), amount) : "-");
        signBuilder.append(currency != null && !currency.isEmpty() ? String.format("%d%s", currency.length(), currency) : "-");
        signBuilder.append(order != null && !order.isEmpty() ? String.format("%d%s", order.length(), order) : "-");
        signBuilder.append(merchName != null && !merchName.isEmpty() ? String.format("%d%s", merchName.length(), merchName) : "-");
        signBuilder.append(merchant != null && !merchant.isEmpty() ? String.format("%d%s", merchant.length(), merchant) : "-");
        signBuilder.append(terminal != null && !terminal.isEmpty() ? String.format("%d%s", terminal.length(), terminal) : "-");
        signBuilder.append(email != null && !email.isEmpty() ? String.format("%d%s", email.length(), email) : "-");
        signBuilder.append(trtype != null && !trtype.isEmpty() ? String.format("%d%s", trtype.length(), trtype) : "-");
        signBuilder.append(timestamp != null && !timestamp.isEmpty() ? String.format("%d%s", timestamp.length(), timestamp) : "-");
        signBuilder.append(nonce != null && !nonce.isEmpty() ? String.format("%d%s", nonce.length(), nonce) : "-");
        signBuilder.append(backref != null && !backref.isEmpty() ? String.format("%d%s", backref.length(), backref) : "-");
        signBuilder.append(result != null && !result.isEmpty() ? String.format("%d%s", result.length(), result) : "-");
        signBuilder.append(rc != null && !rc.isEmpty() ? String.format("%d%s", rc.length(), rc) : "-");
        signBuilder.append(rctext != null && !rctext.isEmpty() ? String.format("%d%s", rctext.length(), rctext) : "-");
        signBuilder.append(authcode != null && !authcode.isEmpty() ? String.format("%d%s", authcode.length(), authcode) : "-");
        signBuilder.append(rrn != null && !rrn.isEmpty() ? String.format("%d%s", rrn.length(), rrn) : "-");
        signBuilder.append(intRef != null && !intRef.isEmpty() ? String.format("%d%s", intRef.length(), intRef) : "-");

        return signBuilder.toString();
    }
}
