package net.astropayments.nbank.payments.models;

public enum HandlerStatuses {
    CHECK,
    DECLINED,
    ACCEPTED,
    TOCHECK,
    MANUAL
}
