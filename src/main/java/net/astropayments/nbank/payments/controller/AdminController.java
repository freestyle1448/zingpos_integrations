package net.astropayments.nbank.payments.controller;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.services.AdminService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;

    @PostMapping("/api/account/sub")
    public Callable<Answer> accountSub(Long amount, String currency, String account) {
        return () -> adminService.accountSub(Balance.builder()
                .amount(amount)
                .currency(currency)
                .build(), account).get();

    }

    @PostMapping("/api/account/add")
    public Callable<Answer> accountAdd(Long amount, String currency, String account, MerchantOperDto operDto) {
        if (operDto.getWithdrawalMethod() == null) {
            return () -> adminService.accountAdd(Balance.builder()
                    .amount(amount)
                    .currency(currency)
                    .build(), account).get();
        } else {
            return () -> adminService.accountAddWithMerchOper(Balance.builder()
                    .amount(amount)
                    .currency(currency)
                    .build(), account, operDto).get();
        }
    }
}
