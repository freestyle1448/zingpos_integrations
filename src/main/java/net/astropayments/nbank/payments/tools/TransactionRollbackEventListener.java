package net.astropayments.nbank.payments.tools;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.TransactionRollbackEvent;
import net.astropayments.nbank.payments.services.SystemService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@RequiredArgsConstructor
public class TransactionRollbackEventListener {
    private final SystemService systemService;

    @TransactionalEventListener(phase = TransactionPhase.AFTER_ROLLBACK, classes = TransactionRollbackEvent.class, condition = "#event.isManualException")
    public void saveTransactionAfterRollback(TransactionRollbackEvent event) {
        final var transaction = event.getTransaction();

        systemService.saveTransaction(transaction, transaction.getStage(), transaction.getStatus(),
                transaction.getErrorReason(), transaction.getSystemError());
    }
}
