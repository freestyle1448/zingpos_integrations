package net.astropayments.nbank.payments.services;

import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.transaction.Balance;

import java.util.concurrent.CompletableFuture;

public interface AdminService {
    CompletableFuture<Answer> accountAdd(Balance balance, String account);

    CompletableFuture<Answer> accountSub(Balance balance, String account);

    CompletableFuture<Answer> accountAddWithMerchOper(Balance balance, String account, MerchantOperDto merchantOperDto);
}
