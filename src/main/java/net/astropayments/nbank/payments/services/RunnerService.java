package net.astropayments.nbank.payments.services;

import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.transaction.Transaction;

public interface RunnerService {
    void withdraw(Transaction transaction, Gate gate);

    void checkTransaction(Transaction transaction, Gate gate);

    void declineTransaction(Transaction transaction, String errorCause, String systemError);

    void ping();
}
