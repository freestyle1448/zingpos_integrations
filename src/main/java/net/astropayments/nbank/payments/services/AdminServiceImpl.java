package net.astropayments.nbank.payments.services;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.exception.NotFoundException;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.nb.Person;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOper;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperData;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.models.transaction.UserCredentials;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.TransactionsRepository;
import net.astropayments.nbank.payments.services.nb.NBHTTPSRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.RequestTypes.IN;
import static net.astropayments.nbank.payments.models.transaction.Status.SUCCESS;

@Async
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final GatesRepository gatesRepository;
    private final NBHTTPSRequest nbhttpsRequest;
    private final ManualRepository manualRepository;
    private final TransactionsRepository transactionsRepository;
    private final SystemService systemService;

    @Recover
    public CompletableFuture<Answer> recoverAccountAdd(Exception ex, Balance balance, String account) {
        return CompletableFuture.completedFuture(Answer.builder()
                .status("ERR")
                .err(ex.getLocalizedMessage())
                .build());
    }

    @Override
    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 500,
            backoff = @Backoff(delay = 5))
    public CompletableFuture<Answer> accountAdd(Balance balance, String account) {
        final var receiverAccount = manualRepository.findAndModifyAccountAdd(account
                , balance);

        if (receiverAccount == null) {
            throw new NotFoundException("Аккаунт с указанным ID не найден!");
        }

        final var transaction = Transaction.builder()
                .startDate(new Date())
                .endDate(new Date())
                .status(SUCCESS)
                .stage(2)
                .amount(balance)
                .finalAmount(balance)
                .type(IN)
                .receiverCredentials(UserCredentials.builder()
                        .account(account)
                        .build())
                .build();

        transactionsRepository.save(transaction);

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(receiverAccount.getBalance().getAmount())
                .build());
    }

    @Override
    public CompletableFuture<Answer> accountSub(Balance balance, String account) {
        logger.info("Обработка операции на списание");

        final var result = systemService.accountSub(Transaction.builder()
                .finalAmount(balance)
                .senderCredentials(UserCredentials.builder()
                        .account(account)
                        .build())
                .build());

        if (result == null) {
            logger.error("При попытке списания с аккаунта произошла ошибка!\n");
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("При попытке списания с аккаунта произошла ошибка!")
                    .build());
        }

        logger.info("Списание с аккаунта успешно");

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(result.getBalance().getAmount())
                .build());
    }

    @Override
    public CompletableFuture<Answer> accountAddWithMerchOper(Balance balance, String account, MerchantOperDto merchantOperDto) {
        final var optionalGate = gatesRepository.findByWithdrawalMethod(merchantOperDto.getWithdrawalMethod());

        if (optionalGate.isEmpty()) {
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("Гейт не найден!")
                    .build());
        }

        final var gate = optionalGate.get();
        var request = ReqMerchantOper.builder()
                .nbCredentials(gate.getNbCredentials())
                .reqId(UUID.randomUUID().toString())
                .signedDataObject(ReqMerchantOperData.builder()
                        .amount((double) (balance.getAmount() / 100))
                        .currencyCode(balance.getCurrency())
                        .dbCr(merchantOperDto.getDbCr())
                        .maskCardNo(merchantOperDto.getMaskCardNo())
                        .merchantAuthCode(merchantOperDto.getMerchantAuthCode())
                        .payer(Person.builder()
                                .lastName(merchantOperDto.getPayerLastName())
                                .build())
                        .pcOrderNo(merchantOperDto.getPcOrderNo())
                        .rrn(merchantOperDto.getRrn())
                        .receiverInn(merchantOperDto.getReceiverInn())
                        .receiverName(merchantOperDto.getReceiverName())
                        .strAccount(merchantOperDto.getStrAccount())
                        .terminalCode(merchantOperDto.getTerminalCode())
                        .url(merchantOperDto.getUrl())
                        .build())
                .build();

        return nbhttpsRequest.reqMerchantOper(request)
                .thenCompose(reqMerchantOperResponse -> {
                    if (reqMerchantOperResponse.getData().getResponseText() == null) {
                        return CompletableFuture.completedFuture(Answer.builder()
                                .status(reqMerchantOperResponse.getData().getStatus())
                                .err(reqMerchantOperResponse.getData().getResponseText())
                                .build());
                    }

                    return accountAdd(balance, account);
                });
    }
}
