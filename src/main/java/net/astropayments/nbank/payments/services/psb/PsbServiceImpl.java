package net.astropayments.nbank.payments.services.psb;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.psb.check.PsbCheckRequest;
import net.astropayments.nbank.payments.models.psb.ping.PsbPingRequest;
import net.astropayments.nbank.payments.models.psb.withdraw.PsbWithdrawRequest;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.services.RunnerService;
import net.astropayments.nbank.payments.services.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

@Service
@Async
@RequiredArgsConstructor
public class PsbServiceImpl implements RunnerService {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");
    private final SimpleDateFormat timestamp = new SimpleDateFormat("yMMddHHmmss");
    private final ManualRepository manualRepository;
    private final SystemService systemService;
    private final PSBHttpRequests psbHttpRequests;
    private final PsbResponseHandler psbResponseHandler;

    {
        timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public void withdraw(Transaction transaction, Gate gate) {
        logger.info("Обработка транзакции на вывод");
        if (transaction.getStage() != 3) {
            final var result = systemService.accountSub(transaction);

            if (result == null) {
                logger.error("При попытке списания средств с аккаунта произошла ошибка!\n {}", transaction);
                return;
            }

            logger.info("Списание с аккаунта успешно");
        }

        final var psbCredentials = gate.getPsbCredentials();
        final var request = PsbWithdrawRequest.builder()
                .AMOUNT(String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100))
                .ORDER(transaction.getTransactionNumber().toString())
                .DESC(transaction.getPurpose())
                .TERMINAL(psbCredentials.getTerminal())
                .MERCH_NAME(psbCredentials.getMerchantName())
                .MERCHANT(psbCredentials.getMerchant())
                .EMAIL(transaction.getReceiverCredentials().getEmail())
                .TIMESTAMP(timestamp.format(new Date()))
                .NONCE(Long.toHexString(new Random(System.currentTimeMillis()).nextLong()))
                .CARD(transaction.getReceiverCredentials().getCardNumber())
                .build();

        psbHttpRequests.payout(request)
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        psbResponseHandler.handleWithdraw(response, transaction);
                    } else {
                        declineTransaction(transaction, "Ошибка при  проведении платежа", "Ошибка при получении ответа от PSB");

                        logger.info("Списание средств неуспешно для транзакции - {}, сообщение - Не удалось получить ответ от PSB"
                                , transaction);
                    }
                });
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {
        final var psbCredentials = gate.getPsbCredentials();
        final var request = PsbCheckRequest.builder()
                .AMOUNT(String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100))
                .ORDER(transaction.getTransactionNumber().toString())
                .DESC(transaction.getPurpose())
                .TERMINAL(psbCredentials.getTerminal())
                .MERCH_NAME(psbCredentials.getMerchantName())
                .MERCHANT(psbCredentials.getMerchant())
                .EMAIL(transaction.getReceiverCredentials().getEmail())
                .TIMESTAMP(timestamp.format(new Date()))
                .NONCE(Long.toHexString(new Random(System.currentTimeMillis()).nextLong()))
                .build();

        psbHttpRequests.check(request)
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        psbResponseHandler.handleStatus(response, transaction);
                    }
                });
    }

    @Override
    public void ping() {
        final var requests = new ArrayList<PsbPingRequest>();
        final var gates = manualRepository.findPsbGates();

        for (Gate gate : gates) {
            final var credentials = gate.getPsbCredentials();
            final var request = PsbPingRequest.builder()
                    .TERMINAL(credentials.getTerminal())
                    .NONCE(Long.toHexString(new Random(System.currentTimeMillis()).nextLong()))
                    .TIMESTAMP(timestamp.format(new Date()))
                    .gateId(gate.getId())
                    .build();

            requests.add(request);
        }

        for (PsbPingRequest request : requests) {
            psbHttpRequests.ping(request).whenComplete((response, throwable) -> {
                if (response != null && throwable == null) {
                    response.setGateId(request.getGateId());
                    psbResponseHandler.handlePing(response);
                }
            });
        }
    }

    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        systemService.declineTransaction(transaction, errorCause, systemError);
    }
}
