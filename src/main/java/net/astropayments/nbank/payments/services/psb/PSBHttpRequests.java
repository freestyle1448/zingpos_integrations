package net.astropayments.nbank.payments.services.psb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.models.psb.check.PsbCheckRequest;
import net.astropayments.nbank.payments.models.psb.check.PsbCheckResponse;
import net.astropayments.nbank.payments.models.psb.ping.PsbPingRequest;
import net.astropayments.nbank.payments.models.psb.ping.PsbPingResponse;
import net.astropayments.nbank.payments.models.psb.properties.PsbProps;
import net.astropayments.nbank.payments.models.psb.withdraw.PsbWithdrawRequest;
import net.astropayments.nbank.payments.models.psb.withdraw.PsbWithdrawResponse;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.RequestTypes.*;

@Component
@RequiredArgsConstructor
public class PSBHttpRequests {
    private static final String LOG_MES = "Запрос - {}\nОтвет - {}\n";
    private static final Logger logger = LoggerFactory.getLogger(PSBHttpRequests.class);

    private final RunnerLogsRepository runnerLogsRepository;
    private final RestTemplate restTemplate;
    private final PsbProps psbProps;
    private final ObjectMapper objectMapper = new JsonMapper();

    public CompletableFuture<PsbWithdrawResponse> payout(PsbWithdrawRequest request) {
        PsbWithdrawResponse response;
        try {
            request.setP_SIGN(createSign(request.createStringSign()));
            final var map = request.requestData();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            final var responseString = restTemplate.postForEntity(psbProps.getUrl().getWithdraw(), entity, String.class);

            if (responseString.equals("{}")) {
                return CompletableFuture.completedFuture(null);
            }

            response = objectMapper.readValue(responseString.getBody(), PsbWithdrawResponse.class);
        } catch (Exception e) {
            logger.error("ошибка при запросе к psb(withdraw)", e);
            runnerLogsRepository.save(RunnerLog.builder()
                    .transactionNumber(request.getORDER())
                    .date(new Date())
                    .requestType(WITHDRAW)
                    .request(request.toString())
                    .fullException(e.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        if (!checkSign(response.getSignData(), response.getPSign())) {
            logger.error("ошибка при запросе к psb(withdraw) - {}", "Подпись не верна!");
            runnerLogsRepository.save(RunnerLog.builder()
                    .transactionNumber(request.getORDER())
                    .date(new Date())
                    .requestType(WITHDRAW)
                    .request(request.toString())
                    .response(response.toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        runnerLogsRepository.save(RunnerLog.builder()
                .transactionNumber(request.getORDER())
                .date(new Date())
                .requestType(WITHDRAW)
                .request(request.toString())
                .response(response.toString())
                .build());
        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<PsbCheckResponse> check(PsbCheckRequest request) {
        PsbCheckResponse response;
        try {
            request.setP_SIGN(createSign(request.createStringSign()));
            final var map = request.requestData();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            HttpEntity<MultiValueMap<String, String>> entity =
                    new HttpEntity<>(map, headers);

            final var responseString = restTemplate.postForObject(psbProps.getUrl().getCheck(), entity, String.class);
            if (responseString.equals("{}")) {
                return CompletableFuture.completedFuture(null);
            }

            response = objectMapper.readValue(responseString, PsbCheckResponse.class);
        } catch (Exception e) {
            logger.error("ошибка при запросе к psb(check)", e);
            runnerLogsRepository.save(RunnerLog.builder()
                    .transactionNumber(request.getORDER())
                    .date(new Date())
                    .requestType(CHECK)
                    .request(request.toString())
                    .fullException(e.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        runnerLogsRepository.save(RunnerLog.builder()
                .transactionNumber(request.getORDER())
                .date(new Date())
                .requestType(CHECK)
                .request(request.toString())
                .response(response.toString())
                .build());
        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<PsbPingResponse> ping(PsbPingRequest request) {
        PsbPingResponse response;
        try {
            request.setP_SIGN(createSign(request.createStringSign()));
            MultiValueMap<String, String> map =
                    new LinkedMultiValueMap<>();
            map.add("TERMINAL", request.getTERMINAL());
            map.add("TIMESTAMP", request.getTIMESTAMP());
            map.add("NONCE", request.getNONCE());
            map.add("P_SIGN", request.getP_SIGN());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            HttpEntity<MultiValueMap<String, String>> entity =
                    new HttpEntity<>(map, headers);

            final var responseString = restTemplate.postForObject(psbProps.getUrl().getPing(), entity, String.class);
            if (responseString.equals("{}")) {
                return CompletableFuture.completedFuture(null);
            }

            response = objectMapper.readValue(responseString, PsbPingResponse.class);
        } catch (Exception e) {
            logger.error("ошибка при запросе к psb(ping)", e);
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(PING)
                    .request(request.toString())
                    .fullException(e.fillInStackTrace().toString())
                    .build());

            return CompletableFuture.completedFuture(null);
        }

        runnerLogsRepository.save(RunnerLog.builder()
                .date(new Date())
                .requestType(PING)
                .request(request.toString())
                .response(response.toString())
                .build());
        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    @SneakyThrows
    private String createSign(String data) {
        java.security.Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );

        StringBuilder pkcs8Lines = new StringBuilder();
        try (BufferedReader rdr = new BufferedReader(new FileReader(psbProps.getFile().getPrivateKeyPath()))) {
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }
        }
        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN RSA PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END RSA PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privKey = kf.generatePrivate(keySpec);
        Signature md5withrsa = Signature.getInstance("MD5withRSA");
        md5withrsa.initSign(privKey);

        md5withrsa.update(data.getBytes());
        byte[] signature = md5withrsa.sign();

        return DatatypeConverter.printHexBinary(signature);
    }

    @SneakyThrows
    private boolean checkSign(String data, String sign) {
        java.security.Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );

        StringBuilder pkcs8Lines = new StringBuilder();
        try (BufferedReader rdr = new BufferedReader(new FileReader(psbProps.getFile().getPublicKeyPath()))) {
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }
        }
        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN PUBLIC KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END PUBLIC KEY-----", "");
        pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey privKey = kf.generatePublic(keySpec);
        Signature md5withrsa = Signature.getInstance("MD5withRSA");
        md5withrsa.initVerify(privKey);
        md5withrsa.update(data.getBytes());

        return md5withrsa.verify(DatatypeConverter.parseHexBinary(sign));
    }
}
