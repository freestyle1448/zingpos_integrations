package net.astropayments.nbank.payments.services;

import net.astropayments.nbank.payments.models.Account;
import net.astropayments.nbank.payments.models.transaction.Transaction;

public interface SystemService {
    Account accountSub(Transaction transaction);

    void saveTransaction(Transaction transaction, int stage, int status, String errorReason, String systemError);

    void confirmTransaction(Transaction transaction);

    void declineTransaction(Transaction transaction, String errorCause, String systemError);
}
