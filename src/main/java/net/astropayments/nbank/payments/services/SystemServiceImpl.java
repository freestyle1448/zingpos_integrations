package net.astropayments.nbank.payments.services;

import com.mongodb.MongoException;
import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.exception.NotEnoughMoneyException;
import net.astropayments.nbank.payments.exception.NotFoundException;
import net.astropayments.nbank.payments.models.Account;
import net.astropayments.nbank.payments.models.transaction.Status;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import net.astropayments.nbank.payments.repositories.TransactionsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static net.astropayments.nbank.payments.models.GateTypes.MANUAL;
import static net.astropayments.nbank.payments.models.transaction.Status.DENIED;
import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;

@Service
@RequiredArgsConstructor
public class SystemServiceImpl implements SystemService {
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final TransactionsRepository transactionsRepository;
    private final ManualRepository manualRepository;

    @Recover
    public Account recoverAccountSub(MongoException mongoException, Transaction transaction) {
        saveTransaction(transaction, 0, WAITING, null, null);
        logger.error("Retry не справился {} \n {}", mongoException, transaction);

        return null;
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 11500,
            backoff = @Backoff(delay = 5))
    @Override
    public Account accountSub(Transaction transaction) {
        final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                , transaction.getFinalAmount());

        if (senderAccount == null) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("Аккаунт отправителя не найден!");
            transaction.setErrorReason("Аккаунт отправителя не найден!");

            throw new NotFoundException("Аккаунт отправителя не найден!");
        }
        if (senderAccount.getBalance().getAmount() < 0) {
            transaction.setStatus(DENIED);
            transaction.setStage(2);
            transaction.setSystemError("INSUFFICIENT_FUNDS");
            transaction.setErrorReason("INSUFFICIENT_FUNDS");

            throw new NotEnoughMoneyException("INSUFFICIENT_FUNDS");
        }

        return senderAccount;
    }

    @Override
    public void saveTransaction(Transaction transaction, int stage, int status, String errorReason, String systemError) {
        if (status == DENIED || status == MANUAL) {
            transaction.setEndDate(new Date());
            transaction.setErrorReason(errorReason);
            transaction.setSystemError(systemError);
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }

    @Override
    public void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            saveTransaction(transaction, 2, Status.SUCCESS, null, null);
            logger.info("Транзакция успешно принята(confirmTransaction) - {}"
                    , transaction);
        } else {
            logger.error("Для подтверждения транзакции передали не существующую транзакцию");
        }
    }

    @Transactional
    @Retryable(value = {MongoException.class, DataAccessException.class},
            maxAttempts = 11500,
            backoff = @Backoff(delay = 5))
    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        final var modifyAccount = manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccount(), transaction.getFinalAmount());

        if (modifyAccount == null) {
            throw new NotFoundException("Аккаунт для возврата не найден!");
        }

        saveTransaction(transaction, 1, Status.DENIED, errorCause, systemError);
    }
}
