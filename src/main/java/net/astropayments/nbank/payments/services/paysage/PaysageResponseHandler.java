package net.astropayments.nbank.payments.services.paysage;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.HandlerStatuses;
import net.astropayments.nbank.payments.models.paysage.payouts.PayoutResponse;
import net.astropayments.nbank.payments.models.paysage.status.StatusResponse;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.services.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static net.astropayments.nbank.payments.models.transaction.Status.IN_PROCESS;

@Component
@RequiredArgsConstructor
public class PaysageResponseHandler {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";
    private static final Logger logger = LoggerFactory.getLogger("net.astropayments.nbank.payments");

    private final SystemService systemService;

    public void handleWithdraw(PayoutResponse response, Transaction transaction) {
        final var message = response.getMessage();
        if (response.getStatus() != null) {
            final var status = response.getStatus();

            switch (checkNBStatus(status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    systemService.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    systemService.saveTransaction(transaction, 3, IN_PROCESS,
                            null, null);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    systemService.saveTransaction(transaction, 1, IN_PROCESS
                            , null, null);
                    break;
                case ACCEPTED:
                    systemService.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        } else {
            logger.info(BAD_MES
                    , transaction, 0, message);
            systemService.declineTransaction(transaction, BANK_DECLINED, message);
        }
    }

    public void handleStatus(StatusResponse response, Transaction transaction) {
        if (response != null) {
            final var message = response.getMessage();
            final var status = response.getStatus();

            switch (checkNBStatus(status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    systemService.declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    systemService.saveTransaction(transaction, 3, IN_PROCESS
                            , null, null);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
                case ACCEPTED:
                    systemService.confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        }
    }

    private HandlerStatuses checkNBStatus(String responseText) {
        if (responseText.equals("incomplete")) {
            return HandlerStatuses.CHECK;
        }
        if (responseText.equals("failed") || responseText.equals("expired")) {
            return HandlerStatuses.DECLINED;
        }
        if (responseText.equals("successful")) {
            return HandlerStatuses.ACCEPTED;
        }

        return HandlerStatuses.DECLINED;
    }
}
